package com.fake.models;


import io.quarkus.hibernate.orm.panache.PanacheEntity;
import jakarta.persistence.Entity;

@Entity
public class Endpoints extends PanacheEntity {
    public String url;
    public String type;
    public String input;
    public String output;
}
