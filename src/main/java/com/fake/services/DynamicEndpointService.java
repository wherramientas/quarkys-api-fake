package com.fake.services;

import java.util.List;

import com.fake.interfaces.IDynamicEndpointService;
import io.quarkus.hibernate.orm.panache.PanacheEntityBase;
import io.smallrye.common.constraint.NotNull;

import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import jakarta.persistence.EntityManager;
import jakarta.transaction.Transactional;

import com.fake.models.Endpoints;
import jakarta.ws.rs.core.Response;

@ApplicationScoped
// Necesario para que Quarkus pueda analizar dinámicamente las clases al ejecutarse en modo nativo
public class DynamicEndpointService implements IDynamicEndpointService {

    @Inject
    EntityManager entityManager;


    @Transactional
    public Endpoints crearEndpoint(Endpoints endpoint) {

        if(endpoint.url.startsWith("/")){
            endpoint.url = endpoint.url.replaceFirst("/","");
        }

        // Colocar en mayuscula el metodo a validar
        endpoint.type = endpoint.type.toUpperCase();

        entityManager.persist(endpoint);

        return endpoint;
    }

    @Transactional
    public Response obtenerEndpoints() {
        var data = Endpoints.listAll();
        System.out.println("Cantidad de Datos: "+ data.size());

        if (data != null){
            return Response.ok(data).build();
        }

        return Response.status(404).entity(List.of()).build();
    }
}
