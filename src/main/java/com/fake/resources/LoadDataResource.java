package com.fake.resources;

import com.fake.models.Endpoints;
import com.fake.services.DynamicEndpointService;
import jakarta.inject.Inject;
import jakarta.json.Json;
import jakarta.json.stream.JsonParser;
import jakarta.json.stream.JsonParserFactory;
import jakarta.ws.rs.*;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;
import org.eclipse.microprofile.openapi.annotations.Operation;
import org.eclipse.microprofile.openapi.annotations.media.Content;
import org.eclipse.microprofile.openapi.annotations.media.Schema;
import org.eclipse.microprofile.openapi.annotations.parameters.RequestBody;
import org.eclipse.microprofile.openapi.annotations.responses.APIResponse;
import org.h2.util.json.JSONObject;

import java.util.List;

import static com.fake.utils.Validators.findEndpoint;

@Path("/load-data") // Define la ruta del endpoint principal como "/load-data"
public class LoadDataResource {

    @Inject
    DynamicEndpointService EndpointService;

    @Path("/list")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response list() {
        return EndpointService.obtenerEndpoints();
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Operation(summary = "Load data and create endpoint",
            description = "Load data from the request and create a new endpoint based on the provided information.")
    @APIResponse(responseCode = "201", description = "Success",
            content = @Content(mediaType = MediaType.APPLICATION_JSON,
                    schema = @Schema(implementation = Endpoints.class)))
    @APIResponse(responseCode = "400", description = "Bad Request",
            content = @Content(mediaType = "application/json"))
    public Endpoints listEndpoints(
            @RequestBody(description = "Data for creating a new endpoint",
                    content = @Content(mediaType = "application/json",
                            schema = @Schema(implementation = Endpoints.class)))
            Endpoints newEndpoint
    ) {
        // Lógica para manejar solicitudes POST.
        return EndpointService.crearEndpoint(newEndpoint);
    }

    @Path("/{url}")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response GETEndpoint(@PathParam("url") String url) {
        // Lógica para manejar solicitudes GET a endpoints dinámicos basados en la información de la base de datos

        List<Object> data = findEndpoint(url, "GET");
        boolean process = (boolean) data.get(0);
        Endpoints endpoint = (Endpoints) data.get(1);

        if (process) {
            var input = endpoint.input;
            var output = endpoint.output;



            return Response
                    .ok()
                    .build();
        }

        return Response.status(Response.Status.BAD_REQUEST).build();
    }
}
