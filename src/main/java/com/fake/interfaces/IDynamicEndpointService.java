package com.fake.interfaces;

import com.fake.models.Endpoints;
import jakarta.ws.rs.core.Response;

import java.util.List;

public interface IDynamicEndpointService {
    Endpoints crearEndpoint(Endpoints endpoint);
    Response obtenerEndpoints();


}
