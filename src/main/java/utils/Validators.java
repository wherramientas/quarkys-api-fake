package utils;

import com.fake.models.Endpoints;

import java.util.Arrays;
import java.util.List;

public class Validators {
    public static boolean UrlEndpoint(String url) {
        Endpoints endpointData = Endpoints.find("url", url).firstResult(); // Busca el registro por la URL
        return endpointData != null;
    }

    public static boolean TypeEndpoint(String type) {
        Endpoints endpointData = Endpoints.find("type", type.toUpperCase()).firstResult(); // Busca el registro por la Typo
        return endpointData != null;
    }

    public static List<Object> findEndpoint(String url, String type) {
        Endpoints endpointData = Endpoints.find("url = ?1 and type = ?2", url, type).firstResult();
        return Arrays.asList(endpointData != null, endpointData);
    }

}