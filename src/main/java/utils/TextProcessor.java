package utils;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Clase para procesar texto que contiene notaciones especiales y generar secuencias numéricas en función de esas notaciones.
 */
public class TextProcessor {

    /**
     * Genera una lista de números en un rango específico con un paso dado.
     *
     * @param min  Valor mínimo del rango.
     * @param max  Valor máximo del rango.
     * @param step Paso entre los números en la secuencia.
     * @return Lista de números generada.
     */
    private static List<Integer> genNumbers(int min, int max, int step) {
        if (step == 0) {
            step = 1;
        }

        List<Integer> numbers = new ArrayList<>();

        if (min < max && step < 0) {
            return numbers;
        }

        if (min < max) {
            for (int i = min; i <= max; i += step) {
                numbers.add(i);
            }
        } else {
            for (int i = min; i >= max; i += step) {
                numbers.add(i);
            }
        }

        return numbers;
    }

    /**
     * Extrae una lista de números a partir de una coincidencia de patrón.
     *
     * @param patternMatch Coincidencia de patrón.
     * @return Lista de números extraída.
     */
    private static List<Integer> extractNumbers(String patternMatch) {
        // Eliminamos los caracteres no necesarios y dividimos en números.
        String extract = patternMatch.replace("$", "").replace("num", "").replace("(", "").replace(")", "");
        String[] values = extract.split(",");
        List<Integer> numbers = new ArrayList<>();

        // Convertimos cada valor de cadena en un entero y lo añadimos a la lista de números.
        for (String value : values) {
            try {
                int number = Integer.parseInt(value.trim());
                numbers.add(number);
            } catch (NumberFormatException e) {
                // En caso de que un valor de cadena no pueda ser convertido en un número, imprimir la pila de seguimiento de la excepción.
                //noinspection CallToPrintStackTrace
                e.printStackTrace();
            }
        }

        return numbers;  // Devolvemos la lista de números.
    }

    /**
     * Genera una secuencia de números basada en una lista de números.
     *
     * @param numbers Lista de números.
     * @return Secuencia de números generada.
     */
    private static List<Integer> generateSequence(List<Integer> numbers) {
        List<Integer> gen = null;

        // Si tenemos 2 números, los usamos como mínimo y máximo para generar la secuencia. El paso se ajusta a 1.
        if (numbers.size() == 2) {
            gen = genNumbers(numbers.get(0), numbers.get(1), 1);
        }
        // Si tenemos 3 números, los usamos como mínimo, máximo y paso para generar la secuencia.
        else if (numbers.size() == 3) {
            gen = genNumbers(numbers.get(0), numbers.get(1), numbers.get(2));
        }

        return gen; // Devuelve la secuencia generada.
    }

    /**
     * Resuelve la secuencia generada y produce la cadena de resultado.
     *
     * @param gen          Secuencia generada.
     * @param patternMatch Coincidencia de patrón.
     * @param result       Resultado actual.
     * @return Resultado con la secuencia resuelta.
     */
    private static String resolveSequence(List<Integer> gen, String patternMatch, String result) {

        // Si la secuencia generada está vacía, reemplaza la notación en la cadena de resultado con el mensaje de error.
        if (Objects.requireNonNull(gen).isEmpty()) {
            result = result.replace(patternMatch, "['Error al leer notacion']");
        }else{
            // Si se generó una secuencia, la reemplazamos en la cadena de resultado.
            result = result.replace(patternMatch, gen.toString());
        }


        return result;
    }

    /**
     * Procesa el texto de entrada y reemplaza las notaciones $num por secuencias numéricas.
     *
     * @param input Texto de entrada que puede contener notaciones $num.
     * @return Texto procesado con las secuencias numéricas generadas.
     */
    public static String process_num(String input) {
        // Define el patrón de búsqueda para las notaciones $num.
        Pattern pattern = Pattern.compile("\\$num+[(-?\\d+,-?\\d+)|(-?\\d+,-?\\d+,-?\\d+)]+\\$");
        Matcher matcher = pattern.matcher(input);
        String result = input;

        // Itera sobre las coincidencias de las notaciones $num.
        while (matcher.find()) {
            List<Integer> numbers = extractNumbers(matcher.group(0));
            List<Integer> gen = generateSequence(numbers);
            result = resolveSequence(gen, matcher.group(0), result);
        }

        return result;
    }

    /**
     * Procesa el texto de entrada aplicando el procesamiento de números.
     *
     * @param input Texto de entrada.
     * @return Texto procesado.
     */
    public static String processText(String input) {
        return process_num(input);
    }

    /**
     * Método principal para probar la funcionalidad de la clase.
     *
     * @param args Argumentos de la línea de comandos (no se utilizan).
     */
    public static void main(String[] args) {
        String input = "{'data':$num(1,100,2)$}, {'data2':$num(5,50)$},{'data3':$num(0,75,-1)$},{'data4':$num(100,50,-2)$}";

        String output = processText(input);
        System.out.println(output);
    }
}

